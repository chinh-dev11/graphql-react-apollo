import React from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import Link from './Link';
import { LINKS_PER_PAGE } from '../constants';

export const FEED_QUERY = gql`
  query FeedQuery($first: Int, $skip: Int, $orderBy: LinkOrderByInput) {
    feed(first: $first, skip: $skip, orderBy: $orderBy) {
      links {
        id
        createdAt
        url
        description
        postedBy {
          id
          name
        }
        votes {
          id
          user {
            id
          }
        }
      }
      count
    }
  }
`;

const NEW_LINKS_SUBSCRIPTION = gql`
  subscription {
    newLink {
      id
      url
      description
      createdAt
      postedBy {
        id
        name
      }
      votes {
        id
        user {
          id
        }
      }
    }
  }
`;

const NEW_VOTES_SUBSCRIPTION = gql`
  subscription {
    newVote {
      id
      link {
        id
        url
        description
        createdAt
        postedBy {
          id
          name
        }
        votes {
          id
          user {
            id
          }
        }
      }
      user {
        id
      }
    }
  }
`;

const LinkList = (props) => {
  const { location, match, history } = { ...props };
  const updateCacheAfterVote = (store, createVote, linkId) => {
    const isNewPage = location.pathname.includes('new');
    const page = parseInt(match.params.page, 10);

    const skip = isNewPage ? (page - 1) * LINKS_PER_PAGE : 0;
    const first = isNewPage ? LINKS_PER_PAGE : 100;
    const orderBy = isNewPage ? 'createdAt_DESC' : null;
    const data = store.readQuery({
      query: FEED_QUERY,
      variables: { first, skip, orderBy },
    });

    const votedLink = data.feed.links.find((link) => link.id === linkId);
    votedLink.votes = createVote.link.votes;

    store.writeQuery({ query: FEED_QUERY, data });
  };

  const subscribeToNewLinks = async (subscribeToMore) => {
    subscribeToMore({
      document: NEW_LINKS_SUBSCRIPTION, // This represents the subscription query itself. In your case, the subscription will fire every time a new link is created.
      // All you’re doing inside updateQuery is retrieving the new link from the received subscriptionData, merging it into the existing list of links and returning the result of this operation.
      // same principle as a Redux reducer
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;

        const { newLink } = subscriptionData.data;
        const exists = prev.feed.links.find(({ id }) => id === newLink.id);
        if (exists) return prev;

        return {
          ...prev,
          feed: {
            links: [newLink, ...prev.feed.links],
            count: prev.feed.links.length + 1,
            __typename: prev.feed.__typename,
          },
        };
      },
    });
  };

  const subscribeToNewVotes = (subscribeToMore) => {
    subscribeToMore({
      document: NEW_VOTES_SUBSCRIPTION,
    });
  };

  const nextPage = (data) => {
    const page = parseInt(match.params.page, 10);
    if (page <= data.feed.count / LINKS_PER_PAGE) {
      const nPage = page + 1;
      history.push(`/new/${nPage}`);
    }
  };

  const previousPage = () => {
    const page = parseInt(match.params.page, 10);
    if (page > 1) {
      const pPage = page - 1;
      history.push(`/new/${pPage}`);
    }
  };

  const getQueryVariables = () => {
    const isNewPage = location.pathname.includes('new');
    const page = parseInt(match.params.page, 10);

    const skip = isNewPage ? (page - 1) * LINKS_PER_PAGE : 0;
    const first = isNewPage ? LINKS_PER_PAGE : 100;
    const orderBy = isNewPage ? 'createdAt_DESC' : null;

    return { first, skip, orderBy };
  };

  const getRankedSortedLinks = (data) => {
    const isNewPage = location.pathname.includes('new');
    if (isNewPage) {
      return data.feed.links;
    }

    const rankedLinks = data.feed.links.slice();
    rankedLinks.sort((l1, l2) => l2.votes.length - l1.votes.length);
    return rankedLinks;
  };

  const generateList = (links, pageIndex) => {
    const RSLinks = getRankedSortedLinks(links);
    return RSLinks.map((link, index) => <li key={link.id}><Link link={link} index={index + pageIndex} updateStoreAfterVote={updateCacheAfterVote} /></li>);
  };

  return (
    <div>
      <Query query={FEED_QUERY} variables={getQueryVariables()}>
        {({
          loading, error, data, subscribeToMore,
        }) => {
          if (loading) return <p>Fetching...</p>;
          if (error) return <p>Error</p>;

          subscribeToNewLinks(subscribeToMore);
          subscribeToNewVotes(subscribeToMore);

          const isNewPage = location.pathname.includes('new');
          const pageIndex = match.params.page
            ? (match.params.page - 1) * LINKS_PER_PAGE
            : 0;
          { /* const linksToRender = this._getLinksToRender(data); */ }
          const linksToRender = generateList(data, pageIndex);

          { /* return <ol>{generateList(data.feed.links)}</ol>; */ }
          return (
            <>
              <ol className="list pl0">{linksToRender}</ol>
              {isNewPage && (
              <div className="flex ml4 mv3 gray">
                <button type="button" className="pointer mr2" onClick={previousPage}>Previous</button>
                <button type="button" className="pointer" onClick={() => nextPage(data)}>Next</button>
              </div>
              )}
            </>
          );
        }}
      </Query>
    </div>

  );
};

export default LinkList;
