import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { AUTH_TOKEN } from '../constants';

const Login = (props) => {
  const { history } = { ...props };
  const initialState = {
    login: true,
    email: '',
    password: '',
    name: '',
  };
  const [login, setlogin] = useState(initialState.login);
  const [email, setemail] = useState(initialState.email);
  const [password, setpassword] = useState(initialState.password);
  const [name, setname] = useState(initialState.name);

  const SIGNUP_MUTATION = gql`
    mutation SignupMutation($email: String!, $password: String!, $name: String!) {
      signup(email: $email, password: $password, name: $name) {
        token
      }
    }
  `;

  const LOGIN_MUTATION = gql`
    mutation LoginMutation($email: String!, $password: String!) {
      login(email: $email, password: $password) {
        token
      }
    }
  `;

  const saveUserData = (token) => {
    localStorage.setItem(AUTH_TOKEN, token);
  };

  const confirm = async (data) => {
    const { token } = login ? data.login : data.signup;
    saveUserData(token);
    history.push('/');
  };

  return (
    <div>
      <h4 className="mv3">{login ? 'Login' : 'Sign Up'}</h4>
      <div className="flex flex-column">
        {!login && (
        <input
          value={name}
          onChange={(e) => setname(e.target.value)}
          type="text"
          placeholder="Your name"
        />
        )}
        <input
          value={email}
          onChange={(e) => setemail(e.target.value)}
          type="text"
          placeholder="Your email address"
        />
        <input
          value={password}
          onChange={(e) => setpassword(e.target.value)}
          type="password"
          placeholder="Choose a safe password"
        />
      </div>
      <div className="flex mt3">
        <Mutation
          mutation={login ? LOGIN_MUTATION : SIGNUP_MUTATION}
          variables={{ email, password, name }}
          onCompleted={(data) => confirm(data)}
        >
          {(mutation) => (
            <button type="button" className="pointer mr2 button" onClick={mutation}>
              {login ? 'login' : 'create account'}
            </button>
          )}
        </Mutation>
        <button type="button" className="pointer button" onClick={() => setlogin(!login)}>
          {login ? 'need to create an account?' : 'already have an account?'}
        </button>
      </div>
    </div>
  );
};

export default Login;
