import React, { useState } from 'react';
import { withApollo } from 'react-apollo';
import gql from 'graphql-tag';
import Link from './Link';

const FEED_SEARCH_QUERY = gql`
  query FeedSearchQuery($filter: String!) {
    feed(filter: $filter) {
      links {
        id
        url
        description
        createdAt
        postedBy {
          id
          name
        }
        votes {
          id
          user {
            id
          }
        }
      }
    }
  }
`;

const Search = (props) => {
  // console.log('props: ', props);
  const { client } = { ...props };
  const initialState = {
    links: [],
    filter: '',
  };
  const [links, setlinks] = useState(initialState.links);
  const [filter, setfilter] = useState(initialState.filter);

  const executeSearch = async () => {
    // const { filter } = this.state
    const result = await client.query({
      query: FEED_SEARCH_QUERY,
      variables: { filter },
    });

    setlinks(result.data.feed.links);
  };

  return (
    <div>
      <div>
        Search
        <input
          type="text"
          onChange={(e) => setfilter(e.target.value)}
        />
        <button type="button" onClick={() => executeSearch()}>OK</button>
      </div>
      {links.map((link, index) => (
        <Link key={link.id} link={link} index={index} />
      ))}
    </div>
  );
};

export default withApollo(Search);
