import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';

import './styles/index.css';

import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { setContext } from 'apollo-link-context';
import { split } from 'apollo-link';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';

import { AUTH_TOKEN } from './constants'; import * as serviceWorker from './serviceWorker';
import App from './components/App';

const httpLink = createHttpLink({
  uri: 'http://localhost:4000',
  // uri: 'https://us1.prisma.sh/chinh-le-f48b10/graphql-react-apollo-server/dev',
});

const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem(AUTH_TOKEN);
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : '',
    },
  };
});

// subscription config
const wsLink = new WebSocketLink({
  uri: 'ws://localhost:4000', // the ws instead of http protocol
  // uri: 'wss://us1.prisma.sh/chinh-le-f48b10/graphql-react-apollo-server/dev', // the ws instead of http protocol
  options: {
    reconnect: true,
    connectionParams: {
      authToken: localStorage.getItem(AUTH_TOKEN), // authenticating the websocket connection with the user’s token
    },
  },
});
/*
split: to “route” a request to a specific middleware link.
- It takes three arguments, the first one is a test function which returns a boolean.
- The remaining two arguments are again of type ApolloLink.
    - If test returns true, the request will be forwarded to the link passed as the second argument.
    - If false, to the third one.
 */
const link = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query);
    return kind === 'OperationDefinition' && operation === 'subscription'; // test if whether the requested operation is subscription
  },
  wsLink, // if subscription, it will be forwarded to the wsLink
  authLink.concat(httpLink), // otherwise (if it’s a query or mutation), the authLink.concat(httpLink) will take care of it
);

const client = new ApolloClient({
  link,
  cache: new InMemoryCache(),
});

ReactDOM.render(
  <Router>
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  </Router>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
